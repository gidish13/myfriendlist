package Fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;
import com.myfriendlist.friendlist.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapters.FriendListAdapter;
import General.FriendListItem;
import General.Utils;


public class FacebookFriendListFragment extends Fragment {

    private ProfilePictureView profilePictureView;
    private List<FriendListItem> m_friendsList = null;
    private FriendListAdapter m_adapter = null;
    private TextView userNameView;
    private UiLifecycleHelper uiHelper;
    private String mUserId = null;
    private Session session = null;
    private ListView mList;
    private Activity mActivity = null;


    public FacebookFriendListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = getActivity();


        uiHelper = new UiLifecycleHelper(mActivity, callback);
        uiHelper.onCreate(savedInstanceState);

        // Check for an open session
        session = Session.getActiveSession();

        m_adapter = new FriendListAdapter(mActivity);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_facebook_friend_list, container, false);
        profilePictureView = (ProfilePictureView) view.findViewById(R.id.selection_profile_pic);
        profilePictureView.setCropped(true);

        userNameView = (TextView) view.findViewById(R.id.selection_user_name);

        mList = (ListView) view.findViewById(android.R.id.list);
        mList.setClickable(false);

        refreshFriendList();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }
    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void getUserData(final Session session) {
        makeMeRequest(session);
        getFacebookFriendList(session);
    }

    private void refreshFriendList() {
        if (m_friendsList != null) {
            m_friendsList.clear();
        }

        if (session != null && session.isOpened()) {
            getUserData(session);
        }

    }

    private void getFacebookFriendList(final Session session){
        Request r = new Request(session, "/me/taggable_friends", null, HttpMethod.GET, new Request.Callback() {
            @Override
            public void onCompleted(Response response) {
                try {
                    GraphObject graphObj = response.getGraphObject();
                    if(graphObj != null){
                        JSONObject jsonObj = graphObj.getInnerJSONObject();
                        ParseFriendListFromJson(jsonObj);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        r.executeAsync();
    }

    private void makeMeRequest(final Session session) {
        // Make an API call to get user data and define a
        // new callback to handle the response.
        Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        // If the response is successful
                        if (session == Session.getActiveSession()) {
                            if (user != null) {

                                mUserId = user.getName();
                                profilePictureView.setProfileId(user.getId());
                                userNameView.setText(user.getName());

                            }
                        }
                        if (response.getError() != null) {
                            // Handle errors, will do so later.
                        }
                    }
                });
        request.executeAsync();
    }

    private void onSessionStateChange(final Session newSession, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // Get the user's data.
            this.session = newSession;
            refreshFriendList();
        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private void ParseFriendListFromJson(JSONObject jsonObject) {
        JSONArray array = null;
        m_friendsList = new ArrayList<FriendListItem>();
        try {
            array = jsonObject.getJSONArray("data");
            for(int i = 0; i < array.length(); i++){
                JSONObject Jobj = (JSONObject)array.get(i);
                String id = Jobj.getString(FriendListItem.ID_NOTE);
                String name = Jobj.getString(FriendListItem.NAME_NOTE);
                JSONObject pic_Jobj = Jobj.getJSONObject(FriendListItem.PICTURE_URL_NOTE);
                Jobj = pic_Jobj.getJSONObject("data");
                String picture_url = Jobj.getString("url");
                FriendListItem f = new FriendListItem(name, id, picture_url);
                m_friendsList.add(f);
            }

            m_adapter.setListItems(m_friendsList);
            mList.setAdapter(m_adapter);

            new LoadFreindProfileImage().execute(m_friendsList.toArray(new FriendListItem[] {}));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // AsyncTask class to download the image and set the image
    private class LoadFreindProfileImage extends AsyncTask<FriendListItem, Void, Void> {
        Map<String, Bitmap> profile_pictures = new HashMap<String, Bitmap>();

        @Override
        protected Void doInBackground(FriendListItem... friendListItems) {

            for (FriendListItem item : friendListItems) {
                Bitmap bm = Utils.fetchPhoto(item.getPictureUrl());
                profile_pictures.put(item.getUserId(), bm);
                m_adapter.setIcons(profile_pictures);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m_adapter.notifyDataSetChanged();
                    }
                });

            }


            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

        }


    }

}
