package Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.widget.ProfilePictureView;
import com.google.android.gms.plus.model.people.Person;
import com.myfriendlist.friendlist.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapters.FriendListAdapter;
import Common.Constans;
import General.FriendListItem;
import General.Utils;


public class GooglePlusFriendListFragment extends Fragment {


    private Activity mActivity;

    private Person mCurrentUser = null;

    private ImageView mUserProfileImage = null;

    private TextView userNameView;

    private ListView mList;

    private List<FriendListItem> m_friendsList = null;

    private FriendListAdapter m_adapter = null;


    public GooglePlusFriendListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        m_adapter = new FriendListAdapter(mActivity);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_facebook_friend_list, container, false);
        ProfilePictureView facebookProfilePicture = (ProfilePictureView) view.findViewById(R.id.selection_profile_pic);
        facebookProfilePicture.setVisibility(View.GONE);

        userNameView = (TextView) view.findViewById(R.id.selection_user_name);
        mUserProfileImage = (ImageView) view.findViewById(R.id.google_profile_pic);
        mUserProfileImage.setVisibility(View.VISIBLE);
        mList = (ListView) view.findViewById(android.R.id.list);
        mList.setClickable(false);

        userNameView.setText(Utils.userName);
        new LoadUserProfileImage(mUserProfileImage).execute(Utils.userImageUrl);
        refreshFriendList();

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void FillFriendsList() {
        Log.d(Constans.TAG, "GooglePlusFriendListFragment::FillFriendsList:: Enter");
        if (Utils.personBuffer.isClosed()) {
            Log.d(Constans.TAG, "GooglePlusFriendListFragment::FillFriendsList:: Buffer is closed, skipping");
            return;
        }
        int count = Utils.personBuffer.getCount();
        if (m_friendsList == null) {
            m_friendsList = new ArrayList<FriendListItem>();
        }
        for (int i = 0; i < count; i++) {
            FriendListItem friendListItem = new FriendListItem(Utils.personBuffer.get(i).getDisplayName(), Utils.personBuffer.get(i).getId(), Utils.personBuffer.get(i).getImage().getUrl());
            m_friendsList.add(friendListItem);
        }
        Utils.personBuffer.close();
    }

    private void refreshFriendList() {
        if (m_friendsList != null) {
            m_friendsList.clear();
        }

        FillFriendsList();
        m_adapter.setListItems(m_friendsList);
        mList.setAdapter(m_adapter);

        new LoadFreindProfileImage().execute(m_friendsList.toArray(new FriendListItem[] {}));

    }

    private class LoadUserProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public LoadUserProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                mIcon11 = Utils.fetchPhoto(urldisplay);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    // AsyncTask class to download the image and set the image
    private class LoadFreindProfileImage extends AsyncTask<FriendListItem, Void, Void> {
        Map<String, Bitmap> profile_pictures = new HashMap<String, Bitmap>();

        @Override
        protected Void doInBackground(FriendListItem... friendListItems) {

            for (FriendListItem item : friendListItems) {
                Bitmap bm = Utils.fetchPhoto(item.getPictureUrl());
                profile_pictures.put(item.getUserId(), bm);
                m_adapter.setIcons(profile_pictures);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m_adapter.notifyDataSetChanged();
                    }
                });
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

        }


    }
}


