package com.myfriendlist.friendlist.Activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.Session;
import com.myfriendlist.friendlist.R;

import Common.Constans;
import Fragments.FacebookFriendListFragment;
import Fragments.GooglePlusFriendListFragment;
import General.Utils;

public class FriendListActivity extends ActionBarActivity {

    private String m_socialAppNName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);

        getActionBar().setTitle("");
        getExtra();
        if(m_socialAppNName.equalsIgnoreCase(Constans.FACEBOOK_APP_NAME)) {
            openFacebookFragment();
        }
        else if(m_socialAppNName.equalsIgnoreCase(Constans.GOOGLE_APP_NAME)) {
            openGooglePlusFragment();
        }

    }

    private void getExtra() {
        Intent intent = getIntent();
        m_socialAppNName =  intent.getStringExtra(Constans.SOCIAL_APP_NAME);
    }

    private void openFacebookFragment() {

        Fragment facebookFragment = new FacebookFriendListFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_placeholder, facebookFragment);
        transaction.addToBackStack(null);

        transaction.commit();

    }

    private void openGooglePlusFragment() {

        Fragment googlePlusFriendListFragment = new GooglePlusFriendListFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_placeholder, googlePlusFriendListFragment);
        transaction.addToBackStack(null);

        transaction.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.friend_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            Utils.fromLogout = true;
            callFacebookLogout();
            callGoogleLogout();
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void callFacebookLogout() {
        Session session = Session.getActiveSession();
        if (session != null) {

            if (!session.isClosed()) {
                session.closeAndClearTokenInformation();
            }
        } else {

            session = new Session(FriendListActivity.this);
            Session.setActiveSession(session);

            session.closeAndClearTokenInformation();
        }

    }

    public void callGoogleLogout() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constans.SIGNOUT_FROM_GOOGLE, true);
        setResult(RESULT_OK, resultIntent);
        finish();

    }

}
