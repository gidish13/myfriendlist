package com.myfriendlist.friendlist.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.myfriendlist.friendlist.R;

import java.util.Arrays;

import Common.Constans;
import General.Utils;


public class LoginActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener,
        ResultCallback<People.LoadPeopleResult>,View.OnClickListener {

    private boolean isResumed = false;
    private boolean mIntentInProgress;
    private UiLifecycleHelper uiHelper;
    private boolean mSignInClicked;
    private static final int RC_SIGN_IN = 0;
    boolean mExplicitSignOut = false;
    boolean mInSignInFlow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        Utils.mGoogleApiClient =  new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

        Session session = Session.getActiveSession();
        if (session.isOpened()) {
            openFriendListActivity(Constans.FACEBOOK_APP_NAME, Constans.FACEBOOK_CODE);
        }
        else {
            LoginButton facebookAuthButton = (LoginButton) findViewById(R.id.ButtonFacebook);
            facebookAuthButton.setReadPermissions(Arrays.asList("user_friends,user_status,read_friendlists,manage_friendlists"));

            SignInButton googleAuthButton = (SignInButton) findViewById(R.id.ButtonGoogle);
            googleAuthButton.setOnClickListener(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        isResumed = true;

    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
        isResumed = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constans.FACEBOOK_CODE) {
            uiHelper.onActivityResult(requestCode, resultCode, data);
        }
        else if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!Utils.mGoogleApiClient.isConnecting()) {
                Utils.mGoogleApiClient.connect();
            }
        }
        else if (requestCode == Constans.GOOGLE_CODE) {
            if(data != null && data.getBooleanExtra(Constans.SIGNOUT_FROM_GOOGLE, false) == true) {
                signOutFromGplus();
            }

        }
    }

    protected void onStart() {
        super.onStart();
        if (!mExplicitSignOut) {
            Utils.mGoogleApiClient.connect();
        }
        mExplicitSignOut = false;
    }

    protected void onStop() {
        super.onStop();
        if (Utils.mGoogleApiClient.isConnected()) {
     //       Utils.mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    private void openFriendListActivity(String socialAppNName, int code) {
        Intent intent = new Intent(this,FriendListActivity.class );
        intent.putExtra(Constans.SOCIAL_APP_NAME,socialAppNName);
        startActivityForResult(intent , code);
    }

    //
    // Start Facebook only APIs
    //
    private Session.StatusCallback callback =
            new Session.StatusCallback() {
                @Override
                public void call(Session session,
                                 SessionState state, Exception exception) {
                    onSessionStateChange(session, state, exception);
                }
            };

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        // Only make changes if the activity is visible
        if (isResumed) {

            if (state.isOpened()) {
                // If the session state is open:
                // Show the authenticated freind list activty
                openFriendListActivity(Constans.FACEBOOK_APP_NAME, Constans.FACEBOOK_CODE);
            }
        }
    }


    //
    // End Facebook APIs only
    //


    //
    // Start Google APIs only
    //

    @Override
    public void onClick(View v) {
        if (!Utils.mGoogleApiClient.isConnecting()) {
            switch (v.getId()) {
                case R.id.ButtonGoogle:
                    signInWithGplus();
                    break;
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        Log.i(Constans.TAG, "onConnected");

        if(Utils.mGoogleApiClient.isConnected()) {
            Log.d(Constans.TAG, "GooglePlusFriendListFragment::onCreateView:: mGoogleApiClient is connected ");
            Plus.PeopleApi.loadVisible(Utils.mGoogleApiClient, null)
                    .setResultCallback(this);
        }
        else {
            Log.d(Constans.TAG, "GooglePlusFriendListFragment::onCreateView:: mGoogleApiClient is not connected ");
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Utils.mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            Utils.mConnectionResult = result;

            if (mSignInClicked) {

                resolveSignInError();
            }
        }

    }

    /**
     * Method to resolve any signin errors
     * */
    private void resolveSignInError() {
        if (Utils.mConnectionResult != null && Utils.mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                Utils.mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                Utils.mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onResult(LoadPeopleResult peopleData) {
        Log.d(Constans.TAG, "LoginActivity::onResult:: Enter");
        if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
            Utils.personBuffer  = peopleData.getPersonBuffer();
        }

        if(Utils.mGoogleApiClient != null) {
            Person mCurrentUser = Plus.PeopleApi.getCurrentPerson(Utils.mGoogleApiClient);
            Utils.userName =  mCurrentUser.getName().getGivenName() + " " + mCurrentUser.getName().getFamilyName() ;
            Utils.userImageUrl = mCurrentUser.getImage().getUrl();
        }

        if(mSignInClicked) {
            openFriendListActivity(Constans.GOOGLE_APP_NAME, Constans.GOOGLE_CODE);
        }
    }

    /**
     * Sign-in into google
     * */
    private void signInWithGplus() {
        if (!Utils.mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    /**
     * Sign-out from google
     * */
    private void signOutFromGplus() {
        if (Utils.mGoogleApiClient.isConnected()) {
            mExplicitSignOut = true;
            Plus.AccountApi.clearDefaultAccount(Utils.mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(Utils.mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.d(Constans.TAG, "LoginActivity::onResult:: User access revoked!");
                            Utils.mGoogleApiClient.connect();
                        }

                    });
            Utils.mGoogleApiClient.disconnect();
        }
    }

    //
    // End Google APIs only
    //
}
