package General;

import android.graphics.Bitmap;

/**
 * Created by Gidi on 07/06/2014.
 */
public class FriendListItem {

    public static String ID_NOTE = "id";
    public static String NAME_NOTE = "name";
    public static String PICTURE_URL_NOTE = "picture";
    private Bitmap m_user_image;
    private String m_user_name;
    private String m_user_id;
    private String m_user_picture_url;
    private int m_requestCode;
    private FriendListItem mAdapter;


    public FriendListItem(Bitmap image, String userName, String userId) {
        super();
        this.m_user_image = image;
        this.m_user_name = userName;
        this.m_user_id = userId;
    }

    public FriendListItem(String userName, String userId, String picture_url) {
        super();
        this.m_user_picture_url = picture_url;
        this.m_user_name = userName;
        this.m_user_id = userId;
    }

    public Bitmap getUserImage() {
        return m_user_image;
    }

    public String getUserId() {
        return m_user_id;
    }

    public String getUserName() {
        return m_user_name;
    }

    public String getPictureUrl() {
        return m_user_picture_url;
    }

}
