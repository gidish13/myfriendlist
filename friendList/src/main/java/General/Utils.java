package General;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.model.people.PersonBuffer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.InputStream;

import Common.Constans;

/**
 * Created by Gidi on 09/06/2014.
 */
public class Utils {

    public static GoogleApiClient mGoogleApiClient;
    public static ConnectionResult mConnectionResult;
    public static boolean fromLogout = false;
    public static PersonBuffer personBuffer;
    public static String userName = "";
    public static String userImageUrl = "";


    public static Bitmap fetchPhoto (String url) {
        Bitmap bm = null;

        Log.d(Constans.TAG, "url: " + url);

        try {
            InputStream in = new java.net.URL(url).openStream();
            bm = BitmapFactory.decodeStream(in);

        }
        catch (Exception e) {
            Log.e(Constans.TAG, e.getMessage(), e);
        }

        return bm;
    }
}
