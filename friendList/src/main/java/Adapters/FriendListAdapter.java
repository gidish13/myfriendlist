package Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.myfriendlist.friendlist.R;

import java.util.ArrayList;
import java.util.HashMap;

import Common.Constans;
import General.FriendListItem;
import General.Utils;

import java.util.List;
import java.util.Map;


/**
 * Created by Gidi on 10/06/2014.
 */
public class FriendListAdapter extends BaseAdapter {

    private List<FriendListItem> m_friendList = null;
    private final Context mContext;
    private ImageView m_image = null;
    private List<FriendListItem> m_list;
    private Map<String, Bitmap> mIcons;
    private LayoutInflater mInflater;
    private Bitmap mDeafultIcon;

    public FriendListAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mDeafultIcon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.fb_empty_profile);
    }

    @Override
    public int getCount() {
        if(m_friendList == null) {
            return 0;
        }
        return m_friendList.size();
    }

    @Override
    public Object getItem(int position) {
        return m_friendList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final AppViewHolder holder;
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.friends_list_item, null);

            holder = new AppViewHolder();
            holder.mUserName = (TextView) convertView.findViewById(R.id.user_name);
            holder.mIcon = (ImageView) convertView.findViewById(R.id.profile_pic);
            convertView.setTag(holder);

        }
        else {
            // reuse/overwrite the view passed
            holder = (AppViewHolder) convertView.getTag();
        }

        FriendListItem item = m_friendList.get(position);
        holder.setUserName(item.getUserName());
        holder.setUserId(item.getUserId());
        if(mIcons == null || mIcons.get(item.getUserId()) == null) {
            holder.setIcon(mDeafultIcon);
        }
        else {
            holder.setIcon(mIcons.get(item.getUserId()));
        }
        return convertView;

    }

    public void setListItems(List<FriendListItem> list) {
        m_friendList = list;
    }

    public void setIcons(Map<String, Bitmap> icons) {
        this.mIcons = icons;

    }

    public class AppViewHolder {

        private ImageView mIcon;
        private TextView mUserName;
        private String mUserId;

        public void setUserName(String name) {
            mUserName.setText(name);
        }

        public void setUserId(String id) {
            mUserId = id;
        }

        public String getUserId() {
            return mUserId;
        }

        public void setIcon(Bitmap icon) {
            if(icon != null) {
                mIcon.setImageBitmap(icon);
            }
        }
    }
}
